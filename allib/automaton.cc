#include "automaton.hh"

#include <iostream>

using namespace allib;

DFA::DFA(State starting_state, State accepting_state)
{
    this->starting_state = starting_state;
    this->accepting_state = accepting_state;
}

bool DFA::add_transition(State origin, State destination, Letter value)
{
    auto key = std::make_pair(origin, value);
    this->transitions[key] = destination;
    return true;
}

void DFA::to_dot(std::string name)
{
    // Simple DOT Header to make the graph prettier.
    std::cout << "digraph " << name << " {" << std::endl;
    std::cout << "    rankdir=LR;" << std::endl;

    // The accepting state is marked as a double circle.
    std::cout << "    node [shape = doublecircle]; " << this->accepting_state
              << ";" << std::endl;

    // We create a dummy state q0 in order to indicate with an arrow
    // the starting state with the transition q0 -> starting_state.
    std::cout << "    node [shape = point]; q0" << std::endl;

    // If the starting state is different from the accepting state then
    // we make sure the starting state is represented (if there is no
    // transition in or out the starting state it is not rendered by Graphviz)
    if (this->starting_state != this->accepting_state)
    {
        std::cout << "    node [shape = circle]; " << this->starting_state
                  << ";" << std::endl;
    }
    else
    {
        std::cout << "    node [shape = circle];" << std::endl;
    }

    // Finally we add the q0 -> starting_state transition.
    std::cout << "    q0 -> " << this->starting_state << ";" << std::endl;

    // We iterate through the transitions for labels.
    for (const auto &n : this->transitions)
    {
        std::cout << "    " << n.first.first << " -> " << n.second
                  << " [label=\"" << n.first.second << "\"]" << std::endl;
    }

    // End of the dot output.
    std::cout << "}" << std::endl;
}

void DFA::to_dot(void)
{
    this->to_dot("automaton");
}

bool DFA::accept(std::vector<Letter> word)
{
    // Current state cursor is on starting_state.
    auto current_state = this->starting_state;
    for (const auto &letter : word)
    {
        // Try to find a transition in the transition map.
        auto next_state_it =
            this->transitions.find(std::make_pair(current_state, letter));
        // If there is no transition, reject the word.
        if (next_state_it == this->transitions.end())
        {
            return false;
        }
        // Otherwise move the state cursor to the next state.
        current_state = next_state_it->second;
    }
    // We finished reading the word, check if the cursor is on the accepting
    // state.
    return current_state == accepting_state;
}
