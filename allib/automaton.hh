#pragma once

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

// TODO Write a proper way to hash pairs or be able to represent
// automatons in a different fashion. This function defines a hash on
// pairs. In this case this shouldn't be too bad because we don't allow
// the same transition to be added twice.
struct pair_hash
{
    template <class T1, class T2>
    std::size_t operator()(const std::pair<T1, T2> &p) const
    {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        return h1 ^ h2;
    }
};

namespace allib
{
    using State = long long;
    using Letter = char;
    class DFA
    {
    private:
        std::unordered_map<std::pair<State, Letter>, State, pair_hash>
            transitions;
        State starting_state;
        State accepting_state;

    public:
        DFA(State starting_state, State accepting_state);
        void to_dot(void);
        void to_dot(std::string name);
        bool add_transition(State origin, State destination, Letter value);
        bool accept(std::vector<Letter> word);
    };
} // namespace allib
